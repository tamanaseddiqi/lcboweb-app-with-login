package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return loginName.matches("^[a-zA-Z][a-zA-Z0-9]{5,}+$");
	}
}
