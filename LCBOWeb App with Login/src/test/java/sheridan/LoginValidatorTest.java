package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsLoginAplhaNumnericStartsWithAphabeticCharRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("Tamana") );
	}

	@Test
	public void testIsLoginAplhaNumnericStartsWithAphabeticCharException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("Tam_ana12$") );
	}

	@Test
	public void testIsLoginAplhaNumnericStartsWithAphabeticCharBoundayIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("Tamana123") );
	}

	@Test
	public void testIsLoginAplhaNumnericStartsWithAphabeticCharBoundayOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("0Tamana123") );
	}

	@Test
	public void testLoginContiansSixCharsRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("TamanaSeddiqi123") );
	}

	@Test
	public void testLoginContiansSixCharsException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("Tam   ") );
	}

	@Test
	public void testLoginContiansSixCharsBoundayIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("Tamana") );
	}

	@Test
	public void testLoginContiansSixCharsBoundayOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("amana") );
	}
}
